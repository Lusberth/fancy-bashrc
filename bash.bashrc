#
# /etc/bash.bashrc
#

# If not running interactively, don't do anything

# Define the colors with tput
COLOR=$(tput setaf 6)
COLOR2=$(tput setaf 2)
COLOR3=$(tput setaf 1)
BOLD=$(tput bold)
reset=$(tput sgr0)

# Some usefull alias
alias ls='ls --color=auto'
alias ll='ls -l'
alias la='ls -a'
alias lla='ls -la'
alias grep='grep --color=auto -i'
alias find='find 2>/dev/null'
alias free='free -h'


[[ $- != *i* ]] && return

# Use red for root user and cyan for other users
[[ $EUID -ne 0 ]] && USRCOLOR=$COLOR2 || USRCOLOR=$COLOR3
PS1='`echo $?` [\[$BOLD\]\[$USRCOLOR\]\u\[$reset\]\[$BOLD\]@\[$COLOR\]\h \W\[$reset\]]\$ '


complete -cf sudo
complete -cf man

PS2='> '
PS3='> '
PS4='+ '

case ${TERM} in
  xterm*|rxvt*|Eterm|aterm|kterm|gnome*)
    PROMPT_COMMAND=${PROMPT_COMMAND:+$PROMPT_COMMAND; }'printf "\033]0;%s@%s:%s\007" "${USER}" "${HOSTNAME%%.*}" "${PWD/#$HOME/\~}"'

    ;;
  screen)
    PROMPT_COMMAND=${PROMPT_COMMAND:+$PROMPT_COMMAND; }'printf "\033_%s@%s:%s\033\\" "${USER}" "${HOSTNAME%%.*}" "${PWD/#$HOME/\~}"'
    ;;
esac

[ -r /usr/share/bash-completion/bash_completion   ] && . /usr/share/bash-completion/bash_completion
